#include <le/glfw/module.hpp>

#include <boost/di.hpp>

using namespace boost::di;
using namespace le;

int main(void)
{
  const auto injector = make_injector(
    glfw::module()
  );

  auto& glfw = injector.create<glfw::iglfw&>();
  auto& window = injector.create<glfw::iwindow&>();

  window.create(640, 480, "hello");
  window.make_context_current();

  while (window.is_open())
  {
    glfw.poll_events();

    if (window.key(glfw::key::Escape))
    {
      window.close();
    }

    window.swap_buffers();
  }

  return 0;
}