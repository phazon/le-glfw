#include <le/glfw/module.hpp>

#include <boost/di.hpp>

#include <iostream>

using namespace boost::di;
using namespace le;
using namespace le::glfw;

template<class... Ts> struct overloaded : Ts...
{
  using Ts::operator()...;
};

template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

struct framebuffer_size_handler: window::event_handler
{
  std::set<event_type> handled(void) const override
  {
    return {framebuffer_size};
  }

  void handle(iwindow& window, const event& event) override
  {
    std::visit(overloaded {
      [](const framebuffer_size_event& e) {
        std::cout
          << "framebuffer size changed to "
          << "(" << e.width << ", " << e.height << ")"
          << std::endl;
      }
    }, event);
  }
};

int main(void)
{
  const auto injector = make_injector(
    glfw::module(),
    bind<window::event_handler*[]>.to<framebuffer_size_handler>()
  );

  auto& glfw = injector.create<glfw::iglfw&>();
  auto& window = injector.create<glfw::iwindow&>();

  window.create(640, 480, "hello");
  window.make_context_current();

  while (window.is_open())
  {
    glfw.poll_events();

    if (window.key(glfw::key::Escape))
    {
      window.close();
    }

    window.swap_buffers();
  }

  return 0;
}