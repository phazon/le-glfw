#pragma once

#include <le/glfw/iglfw.hpp>

namespace le::glfw
{
  class initialiser
  {
    
  public:

    initialiser(iglfw& glfw);

    ~initialiser(void);

    static int count(void);

    static bool initialised(void);

  private:

    static int counter;

    static bool success;

    iglfw& glfw;

  };
}