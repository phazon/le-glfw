#pragma once

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

namespace le::glfw
{
  auto get_proc_address(const char *procedureName)
  {
    return glfwGetProcAddress(procedureName);
  }
}