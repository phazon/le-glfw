#pragma once

#include <le/glfw/glfw.hpp>
#include <le/glfw/window.hpp>

#include <boost/di.hpp>

namespace le::glfw
{
  auto module(void)
  {
    return boost::di::make_injector(
      boost::di::bind<le::glfw::iglfw>.to<le::glfw::glfw>(),
      boost::di::bind<le::glfw::iwindow>.to<le::glfw::window>()
    );
  }
}