#pragma once

#include <le/glfw/iglfw.hpp>
#include <le/glfw/initialiser.hpp>

namespace le::glfw
{
  struct glfw : public iglfw
  {
    
    glfw(void);

    bool init(void) const override;

    void terminate(void) const override;
    
    void window_hint(int hint, int value) const override;

    GLFWwindow *create_window(
      int width,
      int height,
      const char* title,
      GLFWmonitor* monitor = nullptr,
      GLFWwindow* share = nullptr
    ) const override;

    void destroy_window(GLFWwindow *window) const override;

    void set_window_user_pointer(
      GLFWwindow *window,
      void *pointer
    ) const override;

    void *get_window_user_pointer(GLFWwindow *window) const override;

    bool get_window_should_close(GLFWwindow *window) const override;

    void set_window_should_close(
      GLFWwindow *window,
      bool value
    ) const override;

    void poll_events(void) const override;

    key_state key(GLFWwindow *window, le::glfw::key key) const override;

    void make_context_current(GLFWwindow *window) const override;

    void swap_buffers(GLFWwindow *window) const override;

    void set_framebuffer_size_callback(
      GLFWwindow *window,
      framebuffer_size_fn callback
    ) const override;

    std::tuple<int, int> get_framebuffer_size(
      GLFWwindow *window
    ) const override;

  private:

    initialiser initialiser;

  };
}