#pragma once

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

namespace le::glfw
{
  /**
   * Structure containing various Window creation settings.
   */
  struct window_settings
  {
    /**
     * Whether the Window is resizable by the user or not.
     */
    bool resizable = true;

    /**
     * Whether the Window is initially visible or not.
     */
    bool visible = true;

    /**
     * Whether the Window is decorated or not.
     */
    bool decorated = true;

    /**
     * Whether the Window is given focus on creation or not.
     */
    bool focused = true;

    /**
     * Whether the Window automatically iconifies or not.
     */
    bool auto_iconify = true;

    /**
     * Whether the Window is "always on top" or not.
     */
    bool floating = false;

    /**
     * The red component of the bit depth of the default frame_buffer.
     */
    int red_bits = 8;

    /**
     * The green component of the bit depth of the default frame_buffer.
     */
    int green_bits = 8;

    /**
     * The blue component of the bit depth of the default frame_buffer.
     */
    int blue_bits = 8;

    /**
     * The alpha (transparency) component of the bit depth of the default
     * frame_buffer.
     */
    int alpha_bits = 8;

    /**
     * The bit depth of the depth buffer of the default frame_buffer.
     */
    int depth_bits = 24;

    /**
     * The bitch depth of the stencil buffer of the default frame_buffer.
     */
    int stencil_bits = 8;

    /**
     * The red component of the bit depth of the accumulation buffer.
     */
    int accum_red_bits = 0;

    /**
     * The green component of the bit depth of the accumulation buffer.
     */
    int accum_green_bits = 0;

    /**
     * The blue component of the bit depth of the accumulation buffer.
     */
    int accum_blue_bits = 0;

    /**
     * The alpha component of the bit depth of the accumulation buffer.
     */
    int accum_alpha_bits = 0;

    /**
     * Number of auxilary buffers. Don't use this.
     */
    int aux_buffers = 0;

    /**
     * The desired refresh rate for full-screen windows. If set to
     * GLFW_DONT_CARE, the highest available refresh rate will be used.
     * Ignored for windowed-mode windows.
     */
    int refresh_rate = GLFW_DONT_CARE;

    /**
     * The number of samples to use for multisampling.
     */
    int samples = GLFW_DONT_CARE;

    /**
     * Whether to use stereoscopic rendering.
     */
    bool stereoscopic = false;

    /**
     * Whether the frame_buffer should be sRGB capable.
     */
    bool srgb_capable = false;

    /**
     * Whether the frame_buffer should be double-buffered.
     */
    bool double_buffer = true;

    /**
     * Specify the OpenGL api. Either GLFW_OPENGL_API or GLFW_OPENGL_ES_API.
     */
    int client_api = GLFW_OPENGL_API;

    /**
     * Major version of the desired client API.
     */
    int context_version_major = 1;

    /**
     * Minor version of the desired client API.
     */
    int context_version_minor = 0;

    /**
     * The robustness strategy to be used by the OpenGL context.
     * Can be GLFW_NO_RESET_NOTIFICATION, GLFW_LOSE_CONTEXT_ON_RESET,
     * or GLFW_NO_ROBUSTNESS
     */
    int context_robustness = GLFW_NO_ROBUSTNESS;

    /**
     * Specifies the release behaviour to be used by the context.
     * Can be GLFW_ANY_RELEASE_BEHAVIOR, GLFW_RELEASE_BEHAVIOR_FLUSH, or
     * GLFW_RELEASE_BEHAVIOR_NONE.
     */
    int context_release_behaviour = GLFW_ANY_RELEASE_BEHAVIOR;

    /**
     * Specifies whether the OpenGL context should be forward compatible,
     * i.e. remove any deprecated functionality for the requested OpenGL
     * version. Only works if requested API version is 3.0 or above.
     * Also ignored if OpenGL ES is requested.
     */
    bool opengl_forward_compat = false;

    /**
     * Whether to create an OpenGL debug context, providing additional error
     * and performance issue reporting functionality. Ignored if using
     * OpenGL ES.
     */
    bool opengl_debug_context = false;

    /**
     * Specifies the requested OpenGL profile. Can be
     * GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_COMPAT_PROFILE,
     * or GLFW_OPENGL_CORE_PROFILE. Must be GLFW_OPENGL_ANY_PROFILE if
     * context version is below 3.2. Ignored if using OpenGL ES.
     */
    int opengl_profile = GLFW_OPENGL_ANY_PROFILE;
  };

  bool operator==(const window_settings& lhs, const window_settings& rhs);
}
