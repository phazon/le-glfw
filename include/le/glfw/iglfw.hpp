#pragma once

#include <functional>
#include <string>

#include <le/glfw/enum/key.hpp>
#include <le/glfw/enum/key_state.hpp>

struct GLFWwindow;
struct GLFWmonitor;

namespace le::glfw
{
  typedef void (* framebuffer_size_fn)(GLFWwindow *, int, int);
  using get_window_user_pointer_fn = std::function<void *(GLFWwindow *window)>;

  struct iglfw
  {
    virtual bool init(void) const = 0;

    virtual void terminate(void) const = 0;

    virtual void window_hint(int hint, int value) const = 0;

    virtual GLFWwindow *create_window(
      int width,
      int height,
      const char* title,
      GLFWmonitor* monitor = nullptr,
      GLFWwindow* share = nullptr
    ) const = 0;

    virtual void destroy_window(GLFWwindow *window) const = 0;

    virtual void set_window_user_pointer(
      GLFWwindow *window,
      void *pointer
    ) const = 0;

    virtual void *get_window_user_pointer(GLFWwindow *window) const = 0;

    virtual bool get_window_should_close(GLFWwindow *window) const = 0;

    virtual void set_window_should_close(
      GLFWwindow *window,
      bool value
    ) const = 0;

    virtual void poll_events(void) const = 0;

    virtual key_state key(GLFWwindow *window, key key) const = 0;

    virtual void make_context_current(GLFWwindow *window) const = 0;

    virtual void swap_buffers(GLFWwindow *window) const = 0;

    virtual void set_framebuffer_size_callback(
      GLFWwindow *window,
      framebuffer_size_fn callback
    ) const = 0;

    virtual std::tuple<int, int> get_framebuffer_size(
      GLFWwindow *window
    ) const = 0;
  };
}
