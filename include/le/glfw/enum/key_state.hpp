#pragma once

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

namespace le::glfw
{
  enum key_state : int
  {
    /* key_event was released */
    Release = GLFW_RELEASE,

    /* key_event was pressed */
    Press = GLFW_PRESS,

    /* key_event was held until repeated */
    Repeat = GLFW_REPEAT,
  };
}