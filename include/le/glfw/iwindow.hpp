#pragma once

#include <string>

#include <le/glfw/window_settings.hpp>
#include <le/glfw/enum/key.hpp>
#include <le/glfw/enum/key_state.hpp>

namespace le::glfw
{
  struct iwindow
  {
    virtual void hints(const window_settings& settings) const = 0;
    
    virtual void create(
      int width,
      int height,
      const std::string& title,
      const window_settings& settings = window_settings()
    ) = 0;

    virtual void destroy(void) = 0;

    virtual bool is_open(void) const = 0;

    virtual void close(void) const = 0;

    virtual void poll_events(void) const = 0;

    virtual key_state key(key key) const = 0;

    virtual void make_context_current(void) const = 0;

    virtual void swap_buffers(void) const = 0;

    virtual std::tuple<int, int> framebuffer_size() const = 0;
  };
}