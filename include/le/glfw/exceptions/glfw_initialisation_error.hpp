#pragma once  

#include <stdexcept>

namespace le
{
  struct glfw_initialisation_error : std::runtime_error
  {
    glfw_initialisation_error(void) :
      std::runtime_error("glfw Initialisation error")
    { }
  };
}