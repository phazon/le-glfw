#pragma once

#include <le/glfw/iwindow.hpp>
#include <le/glfw/iglfw.hpp>

#include <memory>
#include <set>
#include <variant>
#include <vector>

namespace le::glfw
{
  struct window: public iwindow
  {
    struct event_handler
    {
      enum event_type
      {
        framebuffer_size
      };

      struct framebuffer_size_event
      {
        int width;
        int height;
      };

      using event = std::variant<framebuffer_size_event>;

      virtual std::set<event_type> handled() const = 0;

      virtual void handle(iwindow& window, const event& event) = 0;
    };

    window(
      iglfw& glfw,
      std::vector<std::shared_ptr<event_handler>> event_handlers
    );

    ~window(void);

    void hints(const window_settings& settings) const override;

    void create(
      int width,
      int height,
      const std::string& title,
      const window_settings& settings = window_settings()
    ) override;

    void destroy(void) override;

    bool is_open(void) const override;

    void close(void) const override;

    void poll_events(void) const override;

    key_state key(le::glfw::key key) const override;

    void make_context_current(void) const override;

    void swap_buffers(void) const override;

    void register_handler(std::shared_ptr<event_handler> handler);

    std::tuple<int, int> framebuffer_size() const override;

  private:

    friend class window_event_dispatcher;

    iglfw& glfw;

    std::vector<std::shared_ptr<event_handler>> event_handlers;

    GLFWwindow *window_ptr = nullptr;

    void create_window(
      const int width,
      const int height,
      const std::string& title
    );

    void set_window_user_pointer(void *pointer);

    void register_callbacks(void) const;

    void framebuffer_size_callback(
      int width,
      int height
    );
  };

  struct window_event_dispatcher
  {
    static void framebuffer_size_callback(
      GLFWwindow *window_ptr,
      int width,
      int height
    );

    static void *get_window_user_pointer(GLFWwindow *window);

    inline static get_window_user_pointer_fn get_window_user_pointer_fn = nullptr;
  };
}