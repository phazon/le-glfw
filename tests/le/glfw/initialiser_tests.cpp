#include <catch2/catch.hpp>
#include <boost/di.hpp>
#include <fakeit/fakeit.hpp>

#include <le/glfw/iglfw.hpp>
#include <le/glfw/initialiser.hpp>

using namespace boost::di;
using namespace fakeit;
using namespace le;
using namespace le::glfw;

using initialiser_ptr = std::unique_ptr<initialiser>;

TEST_CASE("initialiser", "[le][le::glfw][le::glfw::initialiser]")
{
  Mock<iglfw> glfw;

  Fake(Method(glfw, terminate));

  const auto injector = make_injector(
    bind<iglfw>.to(glfw.get())
  );

  SECTION("constructor")
  {
    SECTION("initialises glfw on construction if counter is 0")
    {
      When(Method(glfw, init)).AlwaysReturn(true);

      auto initialisers = {
        injector.create<initialiser_ptr>(),
        injector.create<initialiser_ptr>()
      };

      Verify(Method(glfw, init)).Once();

      SECTION("incrementing counter")
      {
        REQUIRE(initialiser::count() == 2);
      }

      SECTION("sets initialised to true on success")
      {
        REQUIRE(initialiser::initialised());
      }
    }

    SECTION("throws if glfw fails to initialise")
    {
      When(Method(glfw, init)).AlwaysReturn(false);

      {
        initialiser_ptr initialiser;

        REQUIRE_THROWS(
          initialiser = injector.create<initialiser_ptr>()
        );

        SECTION("decrementing counter")
        {
          REQUIRE(initialiser->count() == 0);
        }
      }      

      SECTION("does not terminate if not initialised")
      {
        Verify(Method(glfw, terminate)).Never();
      }
    }
  }

  SECTION("destructor")
  {
    When(Method(glfw, init)).AlwaysReturn(true);

    {
      auto initialisers = {
        injector.create<initialiser_ptr>(),
        injector.create<initialiser_ptr>()
      };
    }

    SECTION("terminates glfw when counter is decremented to 0")
    {
      REQUIRE(initialiser::count() == 0);
      Verify(Method(glfw, terminate)).Once();;
    }

    SECTION("sets initialised to false")
    {
      REQUIRE_FALSE(initialiser::initialised());
    }
  }
}