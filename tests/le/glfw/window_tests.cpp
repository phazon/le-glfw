#include <string>
#include <map>

#include <catch2/catch.hpp>
#include <boost/di.hpp>
#include <fakeit/fakeit.hpp>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <le/glfw/iglfw.hpp>
#include <le/glfw/window.hpp>

using namespace std::string_literals;

using namespace boost::di;
using namespace fakeit;

using namespace le;
using namespace le::glfw;

using event = window::event_handler::event;
using event_type = window::event_handler::event_type;
using framebuffer_size_event = window::event_handler::framebuffer_size_event;

TEST_CASE("window", "[le][le::glfw][le::glfw::window]")
{
  const auto width = 640;
  const auto height = 480;
  const auto title = "Test window"s;

  GLFWwindow * const window_ptr = (GLFWwindow *) 0xDEADBEEF;

  Mock<iglfw> glfw;

  Fake(
    Method(glfw, window_hint),
    Method(glfw, set_window_user_pointer),
    Method(glfw, destroy_window),
    Method(glfw, poll_events),
    Method(glfw, set_window_should_close),
    Method(glfw, make_context_current),
    Method(glfw, swap_buffers),
    Method(glfw, set_framebuffer_size_callback)
  );

  When(Method(glfw, create_window)).AlwaysReturn(window_ptr);

  Mock<window::event_handler> event_handler;

  Fake(Method(event_handler, handle));
  When(Method(event_handler, handled)).AlwaysReturn({});

  auto event_handler_ptr = std::shared_ptr<window::event_handler>(
    &event_handler.get(),
    [](auto ptr){ }
  );

  window_settings settings;
  std::map<int, int> hints = {
    {GLFW_RESIZABLE, settings.resizable},
    {GLFW_VISIBLE, settings.visible},
    {GLFW_DECORATED, settings.decorated},
    {GLFW_FOCUSED, settings.focused},
    {GLFW_AUTO_ICONIFY, settings.auto_iconify},
    {GLFW_FLOATING, settings.floating},
    {GLFW_RED_BITS, settings.red_bits},
    {GLFW_GREEN_BITS, settings.green_bits},
    {GLFW_BLUE_BITS, settings.blue_bits},
    {GLFW_ALPHA_BITS, settings.alpha_bits},
    {GLFW_DEPTH_BITS, settings.depth_bits},
    {GLFW_STENCIL_BITS, settings.stencil_bits},
    {GLFW_ACCUM_RED_BITS, settings.accum_red_bits},
    {GLFW_ACCUM_GREEN_BITS, settings.accum_green_bits},
    {GLFW_ACCUM_BLUE_BITS, settings.accum_blue_bits},
    {GLFW_ACCUM_ALPHA_BITS, settings.accum_alpha_bits},
    {GLFW_AUX_BUFFERS, settings.aux_buffers},
    {GLFW_REFRESH_RATE, settings.refresh_rate},
    {GLFW_SAMPLES, settings.samples},
    {GLFW_STEREO, settings.stereoscopic},
    {GLFW_SRGB_CAPABLE, settings.srgb_capable},
    {GLFW_DOUBLEBUFFER, settings.double_buffer},
    {GLFW_CLIENT_API, settings.client_api},
    {GLFW_CONTEXT_VERSION_MAJOR, settings.context_version_major},
    {GLFW_CONTEXT_VERSION_MINOR, settings.context_version_minor},
    {GLFW_CONTEXT_ROBUSTNESS, settings.context_robustness},
    {GLFW_CONTEXT_RELEASE_BEHAVIOR, settings.context_release_behaviour},
    {GLFW_OPENGL_FORWARD_COMPAT, settings.opengl_forward_compat},
    {GLFW_OPENGL_DEBUG_CONTEXT, settings.opengl_debug_context},
    {GLFW_OPENGL_PROFILE, settings.opengl_profile},
  };

  const auto injector = make_injector(
    bind<iglfw>.to(glfw.get())
  );

  auto window = injector.create<le::glfw::window>();

  window_event_dispatcher::get_window_user_pointer_fn = [&](GLFWwindow *)
  {
    return static_cast<void *>(&window);
  };

  Mock<iwindow> spy(window);
  Spy(Method(spy, hints));

  SECTION("hints()")
  {
    SECTION("sets window hints")
    {
      window.hints(settings);

      for(const auto& [hint, value] : hints)
      {
        Verify(Method(glfw, window_hint).Using(hint, value));
      }
    }
  }

  SECTION("create()")
  {
    const auto width = 640;
    const auto height = 480;
    const auto title = "Test window"s;

    window.create(width, height, title, settings);

    SECTION("sets window hints before window creation")
    {
      Verify(
        Method(glfw, window_hint) * hints.size(),
        Method(glfw, create_window)
      );
    }

    SECTION("sets window user pointer to window pointer")
    {
      Verify(
        Method(glfw, set_window_user_pointer).Using(window_ptr, &window)
      );
    }

    SECTION("destroys the existing window")
    {
      window.create(width, height, title, settings);

      Verify(
        Method(glfw, destroy_window).Using(window_ptr),
        Method(glfw, create_window)
      );
    }

    SECTION("registers framebuffer size callback")
    {
      Verify(
        Method(glfw, set_framebuffer_size_callback).Using(
          window_ptr,
          window_event_dispatcher::framebuffer_size_callback
        )
      );
    }
  }

  SECTION("destroy()")
  {
    SECTION("destroys window if created")
    {
      window.create(width, height, title, settings);
      window.destroy();

      Verify(Method(glfw, destroy_window).Using(window_ptr));
    }

    SECTION("does not destroy window if not created")
    {
      window.destroy();

      Verify(Method(glfw, destroy_window)).Never();
    }
  }

  SECTION("destructor")
  {
    window.create(width, height, title, settings);
    window.~window();

    SECTION("calls destroy()")
    {
      Verify(Method(glfw, destroy_window).Using(window_ptr));
    }
  }

  SECTION("is_open()")
  {
    SECTION("returns inverse of window_should_close()")
    {
      SECTION("when returning false")
      {
        When(Method(glfw, get_window_should_close)).Return(false);

        REQUIRE(window.is_open());
      }

      SECTION("when returning true")
      {
        When(Method(glfw, get_window_should_close)).Return(true);

        REQUIRE_FALSE(window.is_open());
      }
    }
  }

  SECTION("poll_events()")
  {
    SECTION("calls glfw poll_events()")
    {
      window.poll_events();

      Verify(Method(glfw, poll_events));
    }
  }

  SECTION("close()")
  {
    window.create(width, height, title, settings);
    window.close();

    SECTION("sets window_should_close to true")
    {
      Verify(Method(glfw, set_window_should_close).Using(window_ptr, true));
    }
  }

  SECTION("key()")
  {
    const auto state = GENERATE(
      key_state::Press,
      key_state::Release,
      key_state::Release
    );

    SECTION("returns given key state for window")
    {
      When(Method(glfw, key)).Return(state);

      REQUIRE(window.key(key::Space) == state);
    }
  }

  SECTION("make_context_current()")
  {
    window.create(width, height, title, settings);
    window.make_context_current();

    SECTION("calls glfw make_context_current()")
    {
      Verify(Method(glfw, make_context_current).Using(window_ptr));
    }
  }

  SECTION("swap_buffers()")
  {
    window.create(width, height, title, settings);
    window.swap_buffers();

    SECTION("calls glfw swap_buffers()")
    {
      Verify(Method(glfw, swap_buffers).Using(window_ptr));
    }
  }

  SECTION("framebuffer_size()")
  {
    const std::tuple<int, int> size = {1, 2};
    
    window.create(width, height, title, settings);
    
    When(Method(glfw, get_framebuffer_size)).Return(size);

    SECTION("returns framebuffer size")
    {
      const auto result = window.framebuffer_size();

      Verify(Method(glfw, get_framebuffer_size).Using(window_ptr));
      REQUIRE(result == size);
    }
  }

  SECTION("event handler")
  {
    window.create(width, height, title, settings);
    window.register_handler(event_handler_ptr);

    SECTION("does not receive framebuffer_size event if not interested")
    {
      window_event_dispatcher::framebuffer_size_callback(
        window_ptr,
        10,
        20
      );

      Verify(Method(event_handler, handle)).Never();
    }

    SECTION("receives framebuffer_size event if interested")
    {
      const auto width = 10;
      const auto height = 10;

      event captured_event;

      When(Method(event_handler, handled))
        .AlwaysReturn({event_type::framebuffer_size});

      When(Method(event_handler, handle))
        .Do([&](iwindow& window_, const event& event_)
        {
          captured_event = event_;
        });

      window_event_dispatcher::framebuffer_size_callback(
        window_ptr,
        width,
        height
      );

      Verify(
        Method(event_handler, handle).Matching(
          [&](iwindow& window_, const event&)
          {
            if (!std::holds_alternative<framebuffer_size_event>(captured_event))
            {
              return false;
            }

            const auto info = std::get<framebuffer_size_event>(captured_event);

            return &window_ == &window
              && info.width == width
              && info.height == height;
          }
        )
      );
    }
  }
}