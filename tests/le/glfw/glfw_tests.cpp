#include <catch2/catch.hpp>
#include <boost/di.hpp>
#include <fakeit/fakeit.hpp>

#include <le/glfw/glfw.hpp>
#include <le/glfw/initialiser.hpp>

using namespace boost::di;
using namespace fakeit;

using namespace le::glfw;

TEST_CASE("glfw", "[le][le::glfw][le::glfw::glfw]")
{
  const auto injector = make_injector(
    bind<iglfw>.to<glfw>()
  );

  auto& glfw = injector.create<iglfw&>();
}