#include <le/glfw/initialiser.hpp>
#include <le/glfw/exceptions/glfw_initialisation_error.hpp>

namespace le::glfw
{
  int initialiser::counter = 0;
  
  bool initialiser::success = false;

  initialiser::initialiser(iglfw& glfw) :
    glfw(glfw)
  {
    if (counter++ != 0)
    {
      return;
    }

    success = glfw.init();

    if (!success)
    {
      counter--;

      throw glfw_initialisation_error();
    }
  }

  initialiser::~initialiser(void)
  {
    if (--counter != 0)
    {
      return;
    }

    if (!initialised())
    {
      return;
    }

    glfw.terminate();

    success = false;
  }

  int initialiser::count(void)
  {
    return counter;
  }

  bool initialiser::initialised(void)
  {
    return success;
  }
}