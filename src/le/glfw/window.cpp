#include <map>

#include <le/glfw/window.hpp>
#include <le/glfw/window_settings.hpp>

namespace le::glfw
{
  window::window(
    iglfw& glfw,
    const std::vector<std::shared_ptr<event_handler>> event_handlers) :
      glfw(glfw),
      event_handlers(event_handlers)
  {

  }

  window::~window(void)
  {
    destroy();
  }

  void window::hints(const window_settings& settings) const
  {
    std::map<int, int> hints = {
      {GLFW_RESIZABLE, settings.resizable},
      {GLFW_VISIBLE, settings.visible},
      {GLFW_DECORATED, settings.decorated},
      {GLFW_FOCUSED, settings.focused},
      {GLFW_AUTO_ICONIFY, settings.auto_iconify},
      {GLFW_FLOATING, settings.floating},
      {GLFW_RED_BITS, settings.red_bits},
      {GLFW_GREEN_BITS, settings.green_bits},
      {GLFW_BLUE_BITS, settings.blue_bits},
      {GLFW_ALPHA_BITS, settings.alpha_bits},
      {GLFW_DEPTH_BITS, settings.depth_bits},
      {GLFW_STENCIL_BITS, settings.stencil_bits},
      {GLFW_ACCUM_RED_BITS, settings.accum_red_bits},
      {GLFW_ACCUM_GREEN_BITS, settings.accum_green_bits},
      {GLFW_ACCUM_BLUE_BITS, settings.accum_blue_bits},
      {GLFW_ACCUM_ALPHA_BITS, settings.accum_alpha_bits},
      {GLFW_AUX_BUFFERS, settings.aux_buffers},
      {GLFW_REFRESH_RATE, settings.refresh_rate},
      {GLFW_SAMPLES, settings.samples},
      {GLFW_STEREO, settings.stereoscopic},
      {GLFW_SRGB_CAPABLE, settings.srgb_capable},
      {GLFW_DOUBLEBUFFER, settings.double_buffer},
      {GLFW_CLIENT_API, settings.client_api},
      {GLFW_CONTEXT_VERSION_MAJOR, settings.context_version_major},
      {GLFW_CONTEXT_VERSION_MINOR, settings.context_version_minor},
      {GLFW_CONTEXT_ROBUSTNESS, settings.context_robustness},
      {GLFW_CONTEXT_RELEASE_BEHAVIOR, settings.context_release_behaviour},
      {GLFW_OPENGL_FORWARD_COMPAT, settings.opengl_forward_compat},
      {GLFW_OPENGL_DEBUG_CONTEXT, settings.opengl_debug_context},
      {GLFW_OPENGL_PROFILE, settings.opengl_profile},
    };

    for(const auto& [hint, value] : hints)
    {
      glfw.window_hint(hint, value);
    }
  }

  void window::create(
    int width,
    int height,
    const std::string& title,
    const window_settings& settings)
  {
    hints(settings);
    destroy();
    create_window(width, height, title);
    register_callbacks();
  }

  void window::destroy(void)
  {
    if (window_ptr)
    {
      glfw.destroy_window(window_ptr);

      window_ptr = nullptr;
    }
  }

  bool window::is_open(void) const
  {
    return !glfw.get_window_should_close(window_ptr);
  }

  void window::close(void) const
  {
    glfw.set_window_should_close(window_ptr, true);
  }

  void window::poll_events(void) const
  {
    glfw.poll_events();
  }

  key_state window::key(le::glfw::key key) const
  {
    return glfw.key(window_ptr, key);
  }

  void window::make_context_current(void) const
  {
    glfw.make_context_current(window_ptr);
  }

  void window::swap_buffers(void) const
  {
    glfw.swap_buffers(window_ptr);
  }

  void window::register_handler(std::shared_ptr<event_handler> handler)
  {
    event_handlers.push_back(handler);
  }

  std::tuple<int, int> window::framebuffer_size() const
  {
    return glfw.get_framebuffer_size(window_ptr);
  }

  void window::create_window(
    const int width,
    const int height,
    const std::string& title)
  {
    window_ptr = glfw.create_window(
      width,
      height,
      title.c_str(),
      nullptr,
      nullptr
    );

    set_window_user_pointer(this);
  }

  void window::set_window_user_pointer(void *pointer)
  {
    glfw.set_window_user_pointer(window_ptr, pointer);
  }

  void window::register_callbacks(void) const
  {
    glfw.set_framebuffer_size_callback(
      window_ptr,
      window_event_dispatcher::framebuffer_size_callback
    );
  }

  void window::framebuffer_size_callback(
    int width,
    int height)
  {
    const event_handler::framebuffer_size_event event = {
      .width = width,
      .height = height
    };

    for (auto& handler : event_handlers)
    {
      if (handler->handled().count(window::event_handler::framebuffer_size))
      {
        handler->handle(*this, event);
      }
    }
  }

  void *window_event_dispatcher::get_window_user_pointer(GLFWwindow *window)
  {
    if (!get_window_user_pointer_fn)
    {
      return glfwGetWindowUserPointer(window);
    }

    return get_window_user_pointer_fn(window);
  }

  void window_event_dispatcher::framebuffer_size_callback(
    GLFWwindow *window_ptr,
    int width,
    int height)
  {
    auto& wrapper = *static_cast<window *>(
      get_window_user_pointer(window_ptr)
    );

    wrapper.framebuffer_size_callback(width, height);
  }
}