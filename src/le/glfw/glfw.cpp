#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <le/glfw/glfw.hpp>

namespace le::glfw
{
  glfw::glfw(void) :
    initialiser(*this)
  {

  }

  bool glfw::init(void) const
  {
    return glfwInit();
  }

  void glfw::terminate(void) const
  {
    glfwTerminate();
  }

  void glfw::window_hint(int hint, int value) const
  {
    glfwWindowHint(hint, value);
  }

  GLFWwindow *glfw::create_window(
    int width,
    int height,
    const char* title,
    GLFWmonitor* monitor,
    GLFWwindow* share) const
  {
    return glfwCreateWindow(width, height, title, monitor, share);
  }

  void glfw::destroy_window(GLFWwindow *window) const
  {
    glfwDestroyWindow(window);
  }

  void glfw::set_window_user_pointer(
    GLFWwindow *window,
    void *pointer) const
  {
    glfwSetWindowUserPointer(window, pointer);
  }

  void *glfw::get_window_user_pointer(GLFWwindow *window) const
  {
    return glfwGetWindowUserPointer(window);
  }

  bool glfw::get_window_should_close(GLFWwindow *window) const
  {
    return glfwWindowShouldClose(window);
  }

  void glfw::set_window_should_close(GLFWwindow *window, bool value) const
  {
    glfwSetWindowShouldClose(window, static_cast<int>(value));
  }

  void glfw::poll_events(void) const
  {
    glfwPollEvents();
  }

  key_state glfw::key(GLFWwindow *window, le::glfw::key key) const
  {
    const auto state = glfwGetKey(window, static_cast<int>(key));

    return static_cast<key_state>(state);
  }

  void glfw::make_context_current(GLFWwindow *window) const
  {
    glfwMakeContextCurrent(window);
  }

  void glfw::swap_buffers(GLFWwindow *window) const
  {
    glfwSwapBuffers(window);
  }

  void glfw::set_framebuffer_size_callback(
    GLFWwindow *window,
    framebuffer_size_fn callback) const
  {
    glfwSetFramebufferSizeCallback(window, callback);
  }

  std::tuple<int, int> glfw::get_framebuffer_size(GLFWwindow *window) const
  {
    int width, height;

    glfwGetFramebufferSize(window, &width, &height);

    return {width, height};
  }
}