#include <le/glfw/window_settings.hpp>

namespace le::glfw
{
  bool operator==(const window_settings& lhs, const window_settings& rhs)
  {
    return lhs.resizable == rhs.resizable
      && lhs.visible == rhs.visible
      && lhs.decorated == rhs.decorated
      && lhs.focused == rhs.focused
      && lhs.auto_iconify == rhs.auto_iconify
      && lhs.floating == rhs.floating
      && lhs.red_bits == rhs.red_bits
      && lhs.green_bits == rhs.green_bits
      && lhs.blue_bits == rhs.blue_bits
      && lhs.alpha_bits == rhs.alpha_bits
      && lhs.depth_bits == rhs.depth_bits
      && lhs.stencil_bits == rhs.stencil_bits
      && lhs.accum_red_bits == rhs.accum_red_bits
      && lhs.accum_green_bits == rhs.accum_green_bits
      && lhs.accum_blue_bits == rhs.accum_blue_bits
      && lhs.accum_alpha_bits == rhs.accum_alpha_bits
      && lhs.aux_buffers == rhs.aux_buffers
      && lhs.refresh_rate == rhs.refresh_rate
      && lhs.stereoscopic == rhs.stereoscopic
      && lhs.srgb_capable == rhs.srgb_capable
      && lhs.srgb_capable == rhs.srgb_capable
      && lhs.double_buffer == rhs.double_buffer
      && lhs.client_api == rhs.client_api
      && lhs.context_version_major == rhs.context_version_major
      && lhs.context_version_minor == rhs.context_version_minor
      && lhs.context_release_behaviour == rhs.context_release_behaviour
      && lhs.opengl_forward_compat == rhs.opengl_forward_compat
      && lhs.opengl_debug_context == rhs.opengl_debug_context
      && lhs.opengl_profile == rhs.opengl_profile;
  }
}